package com.nicat.cartaskhw;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarTaskHwApplication {

    public static void main(String[] args) {
        SpringApplication.run(CarTaskHwApplication.class, args);
    }
}
