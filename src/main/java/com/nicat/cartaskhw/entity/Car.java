package com.nicat.cartaskhw.entity;

import jakarta.persistence.*;
import lombok.Data;

@Data
@Entity
@Table(name = "cars")
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String carColor;
    private String engine;
    private String model;
    private String maker;
    private String year;
}
