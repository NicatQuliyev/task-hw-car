package com.nicat.cartaskhw.controller;

import com.nicat.cartaskhw.dto.CarDto;
import com.nicat.cartaskhw.dto.CreateCarDto;
import com.nicat.cartaskhw.service.CarService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/cars")
@RequiredArgsConstructor
public class CarController {

    private final CarService carService;

    @GetMapping
    public List<CarDto> findAll() {
        return carService.findAll();
    }

    @GetMapping("/{id}")
    public CarDto findById(@PathVariable Integer id) {
        return carService.findById(id);
    }

    @PostMapping
    public void create(@RequestBody CreateCarDto carDto) {
        carService.create(carDto);
    }

    @PutMapping("/{id}")
    public void update(@PathVariable Integer id, @RequestBody CarDto carDto) {
        carService.update(id, carDto);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id) {
        carService.delete(id);
    }

}
