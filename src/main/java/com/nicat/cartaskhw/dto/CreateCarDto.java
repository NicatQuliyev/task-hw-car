package com.nicat.cartaskhw.dto;

import lombok.Data;

@Data
public class CreateCarDto {
    private String color;
    private String engine;
    private String model;
    private String maker;
    private String year;
}
