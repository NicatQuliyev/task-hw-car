package com.nicat.cartaskhw.service;

import com.nicat.cartaskhw.dto.CarDto;
import com.nicat.cartaskhw.dto.CreateCarDto;
import com.nicat.cartaskhw.entity.Car;
import com.nicat.cartaskhw.repository.CarRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CarService {

    private final CarRepository carRepository;

    private final ModelMapper modelMapper;

    public List<CarDto> findAll() {
        return carRepository
                .findAll()
                .stream()
                .map(car -> modelMapper.map(car, CarDto.class))
                .collect(Collectors.toList());
    }

    public CarDto findById(Integer id) {
        Car car = carRepository.findById(id).get();

        CarDto carDto = modelMapper.map(car, CarDto.class);

        return carDto;
    }

    public void create(CreateCarDto carDto) {
        Car car = modelMapper.map(carDto, Car.class);

        carRepository.save(car);
    }

    public void update(Integer id, CarDto carDto) {
        Car car = carRepository.findById(id).get();

        car.setCarColor(carDto.getColor());
        car.setYear(carDto.getYear());
        car.setModel(carDto.getModel());
        car.setEngine(carDto.getEngine());
        car.setMaker(carDto.getMaker());

        carRepository.save(car);
    }


    public void delete(Integer id) {
        carRepository.deleteById(id);
    }
}
